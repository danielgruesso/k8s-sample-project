apiVersion: v1
kind: Service
metadata:
  labels:
    app: ${CI_ENVIRONMENT_SLUG}-nginx
  name: ${CI_ENVIRONMENT_SLUG}-nginx
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: ${CI_ENVIRONMENT_SLUG}-nginx
